---
layout: article
permalink: /
excerpt: CHVote project release notice
tags: CHVote, evoting, opensource, AGPL, release, Geneva, source code, documentation
image:
  feature: header.jpg
---

# State of Geneva official release of the CHVote 2.0 project

The State of Geneva hereby releases all the public material that have been created during the CHVote 2.0 applications
development project. This includes the source code of the applications, the technical documentations, the
functional requirements and the release notes.

Although the project has been discontinued, the released material allows any party to build and run an evoting system
allowing to complete the whole process for a votation: prepare and start a ballot, cast votes, and finally decrypt
the ballot box and get the results. It provides individual and universal verifiability using the security model
including independent control components and insiders threats as required by the
[Federal Chancellery ordinance](https://www.admin.ch/opc/en/classified-compilation/20132343/index.html).

The source code is published under the [AGPL v3 licence](https://www.gnu.org/licenses/agpl-3.0.en.html) so that any 
piece of the work can be reused or studied by any third party having an interest in electronic voting, whether they are
academic partners, researchers, students, citizens or any other public or private organization.

Please note that this release is not part of the current electronic voting service. Refer to
the [released source code and documentation](https://github.com/republique-et-canton-de-geneve/chvote-1-0)
of the CHVote 1.0 system instead.
